﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace TestApp
{
    class Program
    {

        static void Main(string[] args)
        {
            List<Line> figureLines = new List<Line>();
            StreamReader sr = new StreamReader("Table1.txt");
            Point[] figurePoints = new Point[6];
            int i = 0;
            double minX = 7000000, minY = 8000000, maxX = 0, maxY = 0;
            while (!sr.EndOfStream)
            {
                var str = sr.ReadLine();
                string s = str.Substring(0, str.IndexOf(';'));
                double x = Convert.ToDouble(s);
                if (x > maxX)
                    maxX = x;
                else if (x < minX)
                    minX = x;
                s = str.Substring(str.IndexOf(';') + 1);
                double y = double.Parse(s);
                if (y > maxY)
                    maxY = y;
                else if (y < minY)
                    minY = y;
                figurePoints[i] = new Point(x, y);
                i++;
            }
            sr.Close();
            //соединение точек фигуры в линии
            figureLines.Add(new Line(figurePoints[0], figurePoints[5]));
            figureLines.Add(new Line(figurePoints[0], figurePoints[1]));
            figureLines.Add(new Line(figurePoints[1], figurePoints[2]));
            figureLines.Add(new Line(figurePoints[2], figurePoints[3]));
            figureLines.Add(new Line(figurePoints[3], figurePoints[4]));
            figureLines.Add(new Line(figurePoints[4], figurePoints[5]));
            List<Line> lines = new List<Line>();
            sr = new StreamReader("Table2.txt");
            while (!sr.EndOfStream)
            {
                var str = sr.ReadLine();
                string s = str.Substring(0, str.IndexOf(';'));
                str = str.Substring(str.IndexOf(';') + 1);
                double x1 = Convert.ToDouble(s);
                s = str.Substring(0, str.IndexOf(';'));
                str = str.Substring(str.IndexOf(';') + 1);
                double y1 = Convert.ToDouble(s);
                s = str.Substring(0, str.IndexOf(';'));
                str = str.Substring(str.IndexOf(';') + 1);
                double x2 = Convert.ToDouble(s);
                double y2 = Convert.ToDouble(str);
                lines.Add(new Line(x1, y1, x2, y2));
            }
            sr.Close();
            List<List<Point>> CrossPoints = new List<List<Point>>();
            //проверка пересечения всех линий со сторонами фигуры
            foreach (var line in lines)
            {
                var points = new List<Point>();
                foreach (var fLine in figureLines)
                {
                    var resalt = line.FindCrossWithThisLine(fLine);
                    if (resalt.X != 0 && resalt.Y != 0)
                        points.Add(resalt);
                }
                CrossPoints.Add(points);
            }
            double summaryLenghtInside = 0;
            for (int j = 0; j < CrossPoints.Count; j++)
            {
                switch (CrossPoints[j].Count)
                {
                    case 1://если пересечение только в одном месте то определеятся какая из вершин отрезка внутри фигуры
                        if (CrossPoints[j][0].X - maxX < CrossPoints[0][0].X - minX || CrossPoints[j][0].Y - maxY < CrossPoints[0][0].Y - minY)
                        {
                            if (lines[j].X1 < lines[j].X2)
                                summaryLenghtInside += Math.Sqrt(Math.Pow((lines[j].X1 - CrossPoints[j][0].X), 2) + Math.Pow((lines[j].Y1 - CrossPoints[j][0].Y), 2));
                            else
                                summaryLenghtInside += Math.Sqrt(Math.Pow((lines[j].X2 - CrossPoints[j][0].X), 2) + Math.Pow((lines[j].Y2 - CrossPoints[j][0].Y), 2));
                        }
                        else if (CrossPoints[0][0].X - maxX > CrossPoints[0][0].X - minX || CrossPoints[0][0].Y - maxY > CrossPoints[0][0].Y - minY)
                        {
                            if (lines[j].X1 > lines[j].X2)
                                summaryLenghtInside += Math.Sqrt(Math.Pow((lines[j].X1 - CrossPoints[j][0].X), 2) + Math.Pow((lines[j].Y1 - CrossPoints[j][0].Y), 2));
                            else
                                summaryLenghtInside += Math.Sqrt(Math.Pow((lines[j].X2 - CrossPoints[j][0].X), 2) + Math.Pow((lines[j].Y2 - CrossPoints[j][0].Y), 2));
                        }
                        break;
                    case 2://если два пересечения то длина от точки пересечения до точки пересечения
                        summaryLenghtInside += Math.Sqrt(Math.Pow((CrossPoints[j][1].X - CrossPoints[j][0].X), 2) + Math.Pow((CrossPoints[j][1].Y - CrossPoints[j][0].Y), 2));
                        break;
                }
            }
            StreamWriter sw = new StreamWriter("Resalt.txt");
            sw.WriteLine("Resalt of working is: " + summaryLenghtInside);
            sw.Close();
            Console.WriteLine("Resalt of working is: " + summaryLenghtInside);
            Console.ReadKey();
        }


    }
    public struct Line
    {
        public double X1;
        public double X2;
        public double Y1;
        public double Y2;
        public Line(double x1, double y1, double x2, double y2)
        {
            X1 = x1;
            Y1 = y1;
            X2 = x2;
            Y2 = y2;
        }
        public Line(Point p1, Point p2)
        {
            X1 = p1.X;
            Y1 = p1.Y;
            X2 = p2.X;
            Y2 = p2.Y;
        }
        /// <summary>
        /// Поиск Пересечения между линиями
        /// </summary>
        /// <param name="line"> линия с которой ищутся пересечения</param>
        /// <returns></returns>
        public Point FindCrossWithThisLine(Line line)
        {
            double m = X2 - X1,
                n = Y2 - Y1,
                m1 = line.X2 - line.X1,
                n1 = line.Y2 - line.Y1,
                x0 = X1,
                x1 = line.X1,
                y0 = Y1,
                y1 = line.Y1;
            var x = (m * m1 * (y1 - y0) + x0 * m1 * n - x1 * n1 * m) / (n * m1 - n1 * m);
            var y = (n * n1 * (x1 - x0) + y0 * n1 * m - y1 * m1 * n) / (m * n1 - m1 * n);//координаты пересечения прямых 
            var maxX = Math.Max(X1, X2);
            var minX = Math.Min(X1, X2);
            var maxY = Math.Max(Y1, Y2);
            var minY = Math.Min(Y1, Y2);
            var lineMaxX = Math.Max(line.X1, line.X2);
            var lineMinX = Math.Min(line.X1, line.X2);
            var lineMaxY = Math.Max(line.Y1, line.Y2);
            var lineMinY = Math.Min(line.Y1, line.Y2);

            if (x >= minX && x <= maxX && y >= minY && y <= maxY && x >= lineMinX && x <= lineMaxX && y >= lineMinY && y <= lineMaxY)//проверка лежат ли эти точки пересечения на отрезке
            {
                return new Point(x, y);

            }
            return new Point(0, 0);
        }
    }
    public struct Point
    {
        public double X;
        public double Y;
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
